"use strict";

import EventComponent from "../widget/src/event_component.js";

export default class Sidebar extends EventComponent {
    constructor(workflow) {
        super();
        this.workflow = workflow;
        this.element = workflow.element;
        this.objects = {};
        this.generateSidebar();
        this.initializeSidebar();
    }

    addInfo(sstInfo) {
        sstInfo.getElements().forEach( (element) => {

            let name = element.name || "unnamed";
            let option = document.createElement("option");
            option.element = element;
            option.value = name;
            option.text = name;
            this.element_input.appendChild(option);
            this.updateElementsList();
        });
    }

    initializeSidebar() {
        if (this.sidebar) {
            if (this.collapse) {
                this.collapse.addEventListener('click', (event) => {
                    this.collapse.classList.toggle('reveal');
                });

                // If it is editable, reveal the sidebar
                this.collapse.classList.remove('reveal');
            }

            this.element_input.addEventListener("change", () => {
                this.updateComponentsList();
            });

            this.component_input.addEventListener("change", (event) => {
                event.preventDefault();
                this.updateNode();
            });
        }
    }

    generateSidebar() {
        // Sidebar
        this.sidebar = document.createElement("div");
        this.sidebar.classList.add("sidebar");
        this.element.insertBefore(this.sidebar, this.element.firstChild);

        // The collapse arrow
        this.collapse = document.createElement("div");
        this.collapse.classList.add("collapse");
        this.collapse.classList.add("reveal");
        this.element.insertBefore(this.collapse, this.sidebar);

        // Component selection
        let selection = document.createElement("div");
        selection.classList.add("component-selection");
        this.sidebar.appendChild(selection);

        let header_select_component = document.createElement("h2");
        header_select_component.innerHTML = "Select Component";
        selection.appendChild(header_select_component);

        let form = document.createElement("form");
        form.classList.add("widget-selection");
        form.name = " widget-selection";
        selection.appendChild(form);

        // Element
        let by_element = document.createElement("div");
        by_element.classList.add("by-element");
        form.appendChild(by_element);

        let header_by_element = document.createElement("h3");
        header_by_element.innerHTML = "Element";
        by_element.appendChild(header_by_element);

        let p_element = document.createElement("p");
        by_element.appendChild(p_element);

        let element_input = document.createElement("select");
        element_input.name = "element";
        element_input.value = "";
        p_element.appendChild(element_input);

        this.element_input = element_input;

        //Component
        let header_component = document.createElement("h3");
        header_component.innerHTML = "Component";
        form.appendChild(header_component);

        let component_input = document.createElement("select");
        component_input.name = "component";
        component_input.placeholder = "Component name";
        component_input.value = "";
        form.appendChild(component_input);

        this.component_input = component_input;

        // Node
        let component_selected = document.createElement("div");
        component_selected.classList.add("component-selected");
        component_selected.classList.add("disabled"); // Make sure user cannot interact during load

        this.component_selected = component_selected;
        selection.appendChild(component_selected);

        this.node = component_selected;

    }

    updateElementsList() {
        if (this.element_input.selectedIndex === -1) {
            this.element_input.selectedIndex = 0;
        }
        this.updateComponentsList();
    }

    updateComponentsList() {

        let option = this.element_input.options[this.element_input.selectedIndex];
        let element = option.element;

        // Remove all current components in this list
        while (this.component_input.firstChild) {
            this.component_input.removeChild(this.component_input.firstChild);
        }

        // Populate the component list with components from the selected element
        element.getComponents().forEach((component) => {
            let option = document.createElement("option");
            option.value = component.name;
            option.text = component.name;
            this.component_input.appendChild(option);
        });

        this.component_input.selectedIndex = 0;
        this.updateNode();
    }

    updateNode() {

        // Get element information from dropdown
        let elementIndex = this.element_input.selectedIndex;
        let elementOption = this.element_input.options[elementIndex];
        let element = elementOption.element;
        let elementName = elementOption.value;

        // Get component information from dropdown
        let componentIndex = this.component_input.selectedIndex;
        let componentOption = this.component_input.options[componentIndex];
        let componentName = componentOption.value;
        let component = element.getComponentByName(componentName);

        let subcomponent = false;
        // If it's not a Component, it must be a SubComponent
        if (component && (component.length === 0)) {
            component = element.getSubcomponentByName(componentName);
            subcomponent = true;
        }

        // Returns an array, first element should be the one as long as
        //    (sub)component names are unique
        component = component[0];


        // This is the data that will be preserved by the workflow tool
        let node = {};
        node.data = {};
        node.data["sst-component-path"] = element.name + "." + component.name;
        node.data["sst-component-name"] = component.name;
        node.data["sst-is-subcomponent"] = subcomponent;
        node.data = component.appendMetadata(node.data);

        // Type displayed in the node, should be Element?
        node.type = "Component";
        if (subcomponent) {
            // It's nice to know if it's a SubComponent
            node.type = "SubComponent";
        }

        // Name displayed in the node
        node.name = component.name;

        // Icon displayed in the node
        node.icon = "./images/simulator.svg";
        // Ports available in the node
        node.ports = [];

        component.subcomponent_slots.forEach((slot) => {
            node.ports.push({
                name: slot.name,
                type: slot.interface,
                visibility: "hidden",
                data: {
                    // To be improved, this just tells the simulator generator
                    //    to not create a link for these
                    ignore: true
                }
            });
        });

        component.ports.forEach((port) => {
            node.ports.push({
                name: port.name,
                type: port.type,
                visibility: "hidden"
            });
        });

        if (component.interface !== undefined && component.interface !== null) {
            node.ports.push({
                name: "Interface",
                type: component.interface,
                visibility: "visible",
                data: {
                    ignore: true
                }
            });
        }

        // The palette simplifies the integration with the workflow
        this.workflow.palette.updateItem(this.node, node);
        // Enable user interaction with the node
        this.node.classList.remove("disabled");

    }

}
