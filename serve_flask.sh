#! /bin/bash

if [ ! -d widget ]
then
	echo "Installing default widget version"
	echo "Replace the 'widget' folder with your own version if you wish!"
	git clone https://gitlab.com/luisfnqoliveira/workflow-widget widget
fi

if [ ! -d venv ] 
then
	echo "Installing a python virtual environment for flask"
	python3 -m venv venv
	. venv/bin/activate
	pip install flask
fi

. venv/bin/activate


which sst-info > /dev/null
SST_EXISTS=$?

if [ "$SST_EXISTS" != "0" ]
then
	echo ""
	echo "--------------------------------------------------------------------------------------"
	echo "WARNING: SST was not found, you may want to use plugin \"static\" for testing purposes"
	echo "Change the value in file bootstrap.sh"
	echo "--------------------------------------------------------------------------------------"
	echo ""
fi

python plugins/flask/backend/backend.py


