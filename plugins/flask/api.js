/* globals console */
"use strict";

import Util from "../../widget/src/util.js";
import { SSTInfo } from "../../sst/SSTInfo.js";
import {Graph2Simulation} from "../../sst/graph2simulation.js";


export class API {

    constructor() {
        // Figure out which project we're editing...
        this.project = "project_name"
    }

    initializeEditor(sstEditor) {
        let saveButton = document.createElement("div");
        saveButton.classList.add("save");
        sstEditor.addElement(saveButton);
        saveButton.addEventListener("mousedown", () => this.save(sstEditor.workflow, sstEditor.sstInfo));
    }

    openWorkflow() {
        return new Promise( (resolve, reject) => {
            let xhr = Util.getJSON("/projects/" + this.project + "/file/data.json", (json) => {
                resolve(json);
            }, {});
            xhr.addEventListener("load", function(event) {
                if (xhr.readyState == 4 && xhr.status != 200) {
                    resolve({});
                }
            });

        });

    }


    save(workflow, sstInfo) {
        let sim = new Graph2Simulation(workflow.nodes, sstInfo);

        let configurations_json = [];
        let configurations_filename = [];

        let configurations = sim.configurations()
        Object.keys(configurations).forEach( (key, index) => {
            configurations_json.push({});
            configurations_filename.push(key+".json");
            let component = configurations[key];
            Object.keys(component).forEach( (parameter) => {
                configurations_json[index][parameter] = component[parameter].default;
            });

        });


	new Promise( (resolve, reject) => {
        // Save workflow
        // Save simulation
            Util.ajax(  "DELETE", "/projects/" + this.project + "/clean" ,
                    null, (response) => {
                        console.log("Deleting old files");
                        console.log(response);
			resolve();
            });
	}).then( () => {


            Util.post(  "/projects/" + this.project +"/file/save/data.json",
                    JSON.stringify(workflow.toJSON()),
                    (response) => {
                        console.log("Saving: data.json");
                        console.log(JSON.stringify(workflow.toJSON()));
                        console.log(response);
            });
            Util.post(  "/projects/" + this.project + "/file/save/simulation.py",
                    sim.simulation, (response) => {
                        console.log("Saving: simulation.py");
                        console.log(sim.simulation);
                        console.log(response);
            });

            // Save all configurations
            configurations_filename.forEach( (file, idx) => {
                Util.post(  "/projects/" + this.project + "/file/save/" + file,
                        JSON.stringify(configurations_json[idx]),
                        (response) => {
                            console.log("Saving: " + file);
                            console.log(JSON.stringify(configurations_json[idx]));
                            console.log(response);
                });
            });
        });
    }

    getSSTInfo() {
        return new Promise( (resolve, reject) => {
            Util.get("/get_info", (xml) => {
                let sstInfo = new SSTInfo();
                sstInfo.addElementsFromXML(xml);
                resolve(sstInfo);
            }, "xml", {});
        });
    }


}
