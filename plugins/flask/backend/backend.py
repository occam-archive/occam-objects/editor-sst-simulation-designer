import flask
import os
import subprocess

app = flask.Flask(__name__)

# Base dir from flask script
dir_path = os.path.dirname(os.path.realpath(__file__))
base = f"{dir_path}/../../.."
projects_base = f"{base}/sst_projects"


def create_project(project_name):
    path = f"{projects_base}/{project_name}"
    if not os.path.exists(path):
        os.makedirs(path)



# Returns the sst-info xml
@app.route('/get_info')
def get_sst_info():
    try:
        output = subprocess.check_output("sst-info -x -o /tmp/sst-info.xml", shell=True).decode("utf-8")
        return flask.send_from_directory('/tmp', "sst-info.xml")
    except:
        return ""

# List existing projects
@app.route('/projects')
def list_projects():
    try:
        dirs = os.listdir(f"{projects_base}")
    except:
        dirs = []
    return {"projects": dirs };

# Delete generated project outputs
@app.route('/projects/<path:project_name>/clean',methods = ['DELETE'])
def delete_project_contents(project_name):
    create_project(project_name)
    contents = os.listdir(f"{projects_base}/{project_name}")
    for content in contents:
        if content != "data.json":
            os.remove(f"{projects_base}/{project_name}/{content}")
    return {
            "status": "success"
        }

@app.route('/projects/<path:project_name>/file/save/<path:path>',methods = ['POST'])
def save_project_file(project_name, path):
    if flask.request.method == 'POST':
        data = flask.request.data.decode("utf-8")
        dest = f"{projects_base}/{project_name}/{path}"
        create_project(project_name)
        with open(dest, "w") as f:
            f.write(data)
        return {
            "status": "success"
        }
    return {
            "status": "failure"
        }
@app.route('/projects/<path:project_name>/file/<path:path>',methods = ['GET'])
def get_project_file(project_name, path):
    create_project(project_name)
    return flask.send_from_directory(f"{projects_base}/{project_name}", path)

@app.route('/<path:path>',methods = ['GET'])
def file(path):
    print(path)
    return flask.send_from_directory(base, path)



host_inet = "0.0.0.0"
host_port = "8889"

print(f"Listening on {host_inet}:{host_port}")
print(f"Try connecting to 127.0.0.1:{host_port}/index.html")

app.run(host='0.0.0.0', port=8889)
