"use strict";

import Util from "../../widget/src/util.js";


export class OccamObject {

    // Needs id and revision
    constructor(object) {
        this.__object = object;
        this.__cache = {};
    }

    get revision() {
	    return this.__object.revision;
    }

    get id() {
	    return this.__object.id;
    }

    get uid() {
        return this.__object.uid;
    }

    clearCache() {
        this.__cache = {};
    }

    get object() {
        return this.__cache.objectPromise || this.__loadObject();
    }

    __loadObject() {
        this.__cache.objectPromise = new Promise( (success, fail)  => {
            Util.getJSON("/" + this.__object.id + "/" + this.__object.revision + "/raw/object.json", (metadata) => {
                this.__object.info = metadata;
                success(this.__object);
            }, {});
        });

        return this.__cache.objectPromise;
    }

    write_metadata(HTMLElement) {
        HTMLElement["object-id"] = this.id;
        HTMLElement["object-revision"] = this.revision;
        HTMLElement["object-name"] = this.object.name;
        HTMLElement["object-type"] = this.object.type;
        HTMLElement["object-subtype"] = this.object.subtype;
    }
}
