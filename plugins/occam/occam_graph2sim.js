"use strict";

import { Graph2Simulation } from "../../sst/graph2simulation.js";

export default class OccamGraph2Simulation extends Graph2Simulation {
    constructor(graph_nodes, sstInfo) {
        super(graph_nodes, sstInfo);

    }

    __parseConnections() {
    }

    __parseComponent(component, c_idx, SSTInfo) {
    }

    get occam_launch() {
        let launch = '#!/usr/bin/python\n' +
            'import os\n' +
            'import json\n' +
            'from subprocess import call\n' +
            'from collections import OrderedDict\n' +
            '\n' +
            'dir_path = os.path.dirname(os.path.realpath(__file__))\n' +
            '\n' +
            'def read_json(path):\n' +
            '    contents = None\n' +
            '    with open(path) as f:\n' +
            '        contents = json.load(f)\n' +
            '    return contents\n' +
            '\n' +
            'def write_json(path, contents):\n' +
            '    with open(path, "w") as f:\n' +
            '        f.write(\n' +
            '            json.dumps(contents, sort_keys=True, indent=4)\n' +
            '        )\n' +
            '\n' +
            '\n' +
            'manifest = read_json("../task.json")\n' +
            '\n' +
            'simulation = "%s/simulation.py" % (dir_path)\n' +
            '\n' +
            'output_file = "../outputs/0/0/statistics.csv"\n' +
            'command = [ "sst",\n' +
            '            simulation,\n' +
            '            "--",\n';

        let index = 0;
        this._sst_graph.components.forEach( (component) => {
            let name = component.unique_name;
            launch += '            "--' + component.unique_config + '",\n';
            launch += '            ' + 'os.path.join(manifest["inputs"][' + component.node.index + ']["connections"][0]["paths"]["mount"], manifest["inputs"][' + component.node.index + ']["connections"][0]["file"])' + ',\n';
            index++;
        });

        this._sst_graph.components.forEach( (component) => {
            let name = component.unique_name;
            this.getFileParameters(component).forEach( (file) => {
                launch += '            "--' + component.unique_name + "-" + file.name + '",\n';
                launch += '            ' + 'os.path.join(manifest["inputs"][' + component.node.index + ']["connections"][0]["paths"]["mount"], manifest["inputs"][' + component.node.index + ']["connections"][0]["file"])' + ',\n';
                index++;
            });
        });

        launch +=
            '            "--output", output_file\n' +
            '          ]\n' +
            'print(command)\n' +
            'call(command)\n';

        return launch;
    }

    get dependencies() {
        let unique_dependencies = {};
        this._sst_graph.components.forEach((component) => {
            let dependency = {
                id: component.node.data["object-id"],
                revision: component.node.data["object-revision"],
                name: component.node.data["object-name"],
                type: component.node.data["object-type"],
                subtype: component.node.data["object-subtype"]
            };

            unique_dependencies[component.node.data["object-id"]+"@"+component.node.data["object-revision"]] = dependency;
        });

        let dependencies = [];
        Object.keys(unique_dependencies).forEach( (key) => {
            dependencies.push(unique_dependencies[key]);
        });

        return dependencies;
    }


}

