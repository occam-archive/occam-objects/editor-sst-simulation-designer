/* globals console */
"use strict";

import { SSTInfo } from "../../sst/SSTInfo.js";
import { SSTObject } from "./sst.js";
import { searchSST, searchSSTSimulators } from "./search_sst.js";
import Util from "../../widget/src/util.js";
import OccamGraph2Simulation from "./occam_graph2sim.js";

class OccamWidgetInterface {

    constructor() {
        this.initialize_events();
        window.addEventListener('message', (event) => {
            if (event.data.name === 'updateConfiguration') {
                this.updateConfiguration(event);
            }
            else if (event.data.name === 'updateToolbar') {
                this.updateToolbar(event);
            }
            else if (event.data.name === 'updateStatus') {
                this.updateStatus(event);
            }
            else if (event.data.name === 'updateInput') {
                this.updateInput(event);
            }
        });
    }

    register_callback(name, callback) {
        this.trigger_events[name].push(callback);
    }
    register_oneshot_callback(name, callback) {
        this.one_shot_events[name].push(callback);
    }
    initialize_events() {
        this.one_shot_events = {
            updateConfiguration:[],
            updateToolbar:[],
            updateStatus:[],
            updateInput:[],
        };
        this.trigger_events = {
            updateConfiguration:[],
            updateToolbar:[],
            updateStatus:[],
            updateInput:[],
        };
    }

    trigger(name, data) {
        window.parent.postMessage({
            'name': name,
            'data': data
        }, '*');
    }

    updateConfiguration(event) {
        this.one_shot_events.updateConfiguration.forEach( (callback) => {
            callback(event);
        });
        this.one_shot_events.updateConfiguration = [];
        this.trigger_events.updateConfiguration.forEach( (callback) => {
            callback(event);
        });

    }

    updateToolbar(event) {
        this.one_shot_events.updateToolbar.forEach( (callback) => {
            callback(event);
        });
        this.one_shot_events.updateToolbar = [];
        this.trigger_events.updateToolbar.forEach( (callback) => {
            callback(event);
        });
    }

    updateStatus(event) {
        this.one_shot_events.updateStatus.forEach( (callback) => {
            callback(event);
        });
        this.one_shot_events.updateStatus = [];
        this.trigger_events.updateStatus.forEach( (callback) => {
            callback(event);
        });

    }

    updateInput(event) {
        this.one_shot_events.updateInput.forEach( (callback) => {
            callback(event);
        });
        this.one_shot_events.updateInput = [];
        this.trigger_events.updateInput.forEach( (callback) => {
            callback(event);
        });
    }

}

export class API {

    constructor() {

        this.toolbar = {
            items: [
                {
                    type: "button",
                    file: "images/floppy.small.svg",
                    tooltip: "Save",
                    name: "save",
                    disabled: true
                }
            ]
        };


        this.isOccam = true;
        this.occamInterface = new OccamWidgetInterface();
    }

    initializeEditor(sstEditor) {
        sstEditor.workflow.on("port-show", (port) => {
            let re = new RegExp("\%\\([^\)]+\\)d");
            let match = port.name.match(re);
            let node = port.node;

            if (match) {
                // Determine the index for this port
                node._sst_ports = node._sst_ports || {};
                let newIndex = node._sst_ports[port.name] || 0;
                node._sst_ports[port.name] = newIndex + 1;

                // Rename this port to an appropriate index
                let newName = port.name.substring(0, match.index) + newIndex
                            + port.name.substring(match.index + match[0].length);
                let info = port.toJSON();
                info.name = newName;

                // Re-hide the port
                port.visibility = "hidden";

                // Add the port
                let newPort = node.createPort("ports", info);
            }
        });

        sstEditor.workflow.on("button-click", (event) => {
            console.log(event);
        });

        this.occamInterface.register_callback("updateToolbar", (event) => {
            if (event.data.data.name === 'save') {
                this.save(sstEditor.workflow, sstEditor.sstInfo);
            }
        });

    }

    openWorkflow() {
        return new Promise( (resolve, reject) => {
            this.occamInterface.register_oneshot_callback("updateInput", (event) => {
                this.__open_input(event, resolve);
            });
            // Request input file (if any)
            this.occamInterface.trigger("updateInput", {});
        });
    }

    __updateToolbar() {
        if (this.editable) {
            this.toolbar.items[0].disabled = !this.editable;

            window.parent.postMessage({
                'name': 'updateToolbar',
                'data': this.toolbar
            }, '*');
        }
        else {
            window.parent.postMessage({
                'name': 'updateToolbar',
                'data': { items: [] }
            }, '*');
        }
    }

    __open_input(event, callback) {
        this.objectInfo = event.data.data;

        if (this.objectInfo.link) {
            // Ensure link is not preserved on save
            delete this.objectInfo.link;

            // This object is staged and editable.
            this.editable = true;
        }
        else {
            this.editable = false;
        }
        this.__updateToolbar();


        // Get the filename to pull the data
        if (this.objectInfo.file !== undefined) {
            var url = "/" + this.objectInfo.id +
                "/" + this.objectInfo.revision +
                "/raw/" + this.objectInfo.file;
            url = this.objectInfo.url || url;

            // Pull the data
            Util.getJSON(url, function(json) {
                callback(json);
            });
        } else {
            // Assume no input is editable for the sake of displaying a nice
            // default for the main view.
            this.editable = true;
            callback({});
        }

    }

    save(workflow, sstInfo) {
        let object_info = this.objectInfo;
        let {id, revision, inputs, outputs, dependencies, run, ...new_object_info} = object_info;

        new_object_info.outputs = [{
            name: "statistics",
            type: "text",
            subtype: "text/csv",
            file: "statistics.csv"
        }];

        new_object_info.architecture = "x86-64";
        new_object_info.environment = "linux";
        new_object_info.run = {
            command: ["/usr/bin/python", "{{ paths.mount }}/launch.py"]
        };
        new_object_info.file = object_info.file;

        let files = [];
        let data = [];

        let serialized = workflow.toJSON();
        let json = JSON.stringify(serialized);
        files.push(new_object_info.file);
        data.push(json);

        let sim = new OccamGraph2Simulation(workflow.nodes, sstInfo);

        let simulation = sim.simulation;
        files.push("simulation.py");
        data.push(simulation);

        let launch = sim.occam_launch;
        files.push("launch.py");
        data.push(launch);

        let sim_dependencies = sim.dependencies;
        new_object_info.dependencies = [];
        sim_dependencies.forEach( (dependency) => {
            new_object_info.dependencies.push(dependency);
        });

        let sim_inputs = sim.inputs;
        if (sim_inputs.length !== 0) {
            new_object_info.inputs = [];
        }
        sim_inputs.forEach((input) => {
            if(input.schema !== undefined) {
                files.push(input.schema.fileName);
                data.push(input.schema.data);
            }
            new_object_info.inputs.push(input.metadata);
        });

        files.push("object.json");
        data.push(JSON.stringify(new_object_info, null, 2));

        files.forEach( (file, i) => {
            window.parent.postMessage({
                'name': 'updateFile',
                'file': file,
                'data': data[i]
            }, '*');
        });

    }

    getSSTInfo() {

        return new Promise( (resolve, reject) => {

            let sst;
            let sstInfo = new SSTInfo();
            // Search for SST in Occam
            searchSST()

            // If found, continue
            .then( (search_result) => {
                if (search_result.objects.length > 0) {
                    // Get first SST found, may change this latter e.g. when we need a specific version
                    let found_sst = search_result.objects[0];
                    sst = new SSTObject(found_sst);
                    // Now, let's search for SST subtype objects
                    return searchSSTSimulators();
                }
                throw new Error("Could not find SST.");
            })

            // Process each found simulator
            .then( (objects) => {
                let promises = [];
                objects.forEach((object) => {
                    promises.push(new Promise( (resolve, reject) => {
                        let sstObject = new SSTObject(object);
                        sstObject.SSTInfo.then( (sstInfo) => {
                            resolve(sstInfo);
                        }).catch( (e) => {
                            resolve(null);
                        });
                    }));
                });
                return Promise.all(promises);
            })
            .then ( (sstInfos) => {
                sstInfos.forEach( (info) => {
                    if( info ) {
                        info.getElements().forEach((element) => {
                            sstInfo.addElement(element);
                        });
                    }
                });
                resolve(sstInfo);
            })
            // If there is an error, abort!
            .catch( (error) => {
                console.log("Error on editor: " + error.message);
                reject();
            });
        });
    }

}
