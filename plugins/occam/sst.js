"use strict";
import { SSTInfo } from "../../sst/SSTInfo.js"
import Util from "../../widget/src/util.js";
import { OccamObject } from "./occam.js";

// Class representing an Occam object with subtype==SST
export class SSTObject extends OccamObject {
    constructor(object) {
        super(object);
        this.__SSTInfo = undefined;
    }

    get SSTInfo() {
        return this.__cache.SSTInfoPromise || this.__loadSSTInfo();
    }

    __loadSSTInfo() {
        this.__cache.SSTInfoPromise =  this.object.then( (object) => {
            if (this.__cache.SSTInfo === undefined) {
                return new Promise( (success, reject) => {
                    let file = object.info.file || "sst-info.xml";
                    Util.get("/" + object.id + "/" + object.revision + "/builds/raw/" + file, (xml) => {
                        // This will be stored in the workflow
                        //   we'll need this when saving
                        let metadata = {};
                        metadata["object-id"] = object.id;
                        metadata["object-revision"] = object.revision;
                        metadata["object-name"] = object.name;
                        metadata["object-type"] = object.type;
                        metadata["object-subtype"] = object.subtype;
                        this.__cache.SSTInfo = new SSTInfo();
                        this.__cache.SSTInfo.addElementsFromXML(xml, metadata);
                        success(this.__cache.SSTInfo);
                    }, "xml", {});
                });
            }
            else {
                return this.__cache.SSTInfo;
            }
        });

        return this.__cache.SSTInfoPromise;
    }


}

export default SSTObject;
