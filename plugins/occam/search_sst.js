"use strict";

import Util from "../../widget/src/util.js";

export function searchSST() {
    return new Promise( (resolve, reject) => {
        let url = "/search";
        let options = {
            query: "SST",
            type: "framework"
        };

        Util.getJSON(url, (json) => {
            if (resolve !== undefined) {
                // TODO: Check versions
                resolve(json);
            }
        }, options);
    });
}

export function searchSSTSimulators() {
    return new Promise( (resolve, reject) => {
        let url = "/search";
        let options = {
            type: "component",
            subtype: "SST"
        };

        Util.getJSON(url, (search_result) => {
            if (resolve !== undefined) {
                if (search_result.objects.length > 0) {
                    resolve(search_result.objects);
                }

                resolve([]);
            }
        }, options);
    });
}

