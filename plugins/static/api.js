/* globals console */
"use strict";

import Util from "../../widget/src/util.js";
import { SSTInfo } from "../../sst/SSTInfo.js";
import {Graph2Simulation} from "../../sst/graph2simulation.js";

export class API {

    save(workflow, sstInfo) {
        let sim = new Graph2Simulation(workflow.nodes, sstInfo);

        let configurations_json = [];
        let configurations_filename = [];

        let configurations = sim.configurations()
        Object.keys(configurations).forEach( (key, index) => {
            configurations_json.push({});
            configurations_filename.push(key+".json");
            let component = configurations[key];
            Object.keys(component).forEach( (parameter) => {
                configurations_json[index][parameter] = component[parameter].default;
            });

        });

        // Save workflow
        console.log("(NOT) Saving: data.json");
        console.log(JSON.stringify(workflow.toJSON()));

        // Save simulation
        console.log("(NOT) Saving: simulation.py");
        console.log(sim.simulation);

        // Save all configurations
        configurations_filename.forEach( (file, idx) => {
            console.log("(NOT) Saving: " + file);
            console.log(JSON.stringify(configurations_json[idx]));
        });
    }

    initializeEditor(sstEditor) {
        let saveButton = document.createElement("div");
        saveButton.classList.add("save");
        sstEditor.addElement(saveButton);
        saveButton.addEventListener("mousedown", () => this.save(sstEditor.workflow, sstEditor.sstInfo));
    }

    openWorkflow() {

        return new Promise( (resolve, reject) => {
            Util.getJSON("./plugins/static/data.json", (json) => {
                resolve(json);
            }, {});
        });

    }

    getSSTInfo() {
        return new Promise( (resolve, reject) => {
            Util.get("./plugins/static/sst-info.xml", (xml) => {
                let sstInfo = new SSTInfo();
                sstInfo.addElementsFromXML(xml);
                resolve(sstInfo);
            }, "xml", {});
        });
    }


}
