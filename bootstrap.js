/* globals console */
"use strict";

import { SSTInfo } from "./sst/SSTInfo.js";
import SSTEditor from "./sst_editor.js"

// Load the plugin API
let config = {
    plugin: "occam"
}
let api_file = await import("./plugins/" + config.plugin + "/api.js");
let api = new api_file.API();

let sstEditor = new SSTEditor(api);


