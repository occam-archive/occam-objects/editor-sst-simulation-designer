"use strict";

import Workflow from "./widget/src/workflow.js";
import Sidebar from "./sidebar/sidebar.js"


class SSTEditor {
    constructor(api) {

        this.api = api;
        this.workflow_element = document.querySelectorAll("workflow-widget")[0];
        this.data = {};

        this.workflow = new Workflow(this.workflow_element,
            {
                buttons:[],
            },
            this.data
        );
        this.sidebar = new Sidebar(this.workflow);

        this.api.initializeEditor(this)


        this.api.openWorkflow()
        .then( (wf) => {this.data = wf;} )
        .catch( (e) => {} )
        .finally( () => {
            this.workflow.fromJSON(this.data);
        });

        this.sstInfo;

        this.api.getSSTInfo()
        .then( (info) => {
            this.sstInfo = info;
            console.log(this.sstInfo.getElements());
            this.sidebar.addInfo(this.sstInfo);
        });


    }

    addElement(element) {
        this.workflow_element.appendChild(element);
    }
}

export default SSTEditor;
