"use strict";

export class SSTGraph {

    // Create class with graph (simulation design) and sstInfo (elements/components/etc)
    constructor( graph_nodes, sstInfo) {
        this.sstInfo = sstInfo;
        this._graph_nodes = graph_nodes;
        this._connections = [];
        this._components = [];

        this.__parseConnections();

    }

    get connections() {
        return this._connections;
    }

    get components() {
        return this._components;
    }

    __parseConnections() {
        this._graph_nodes.forEach((node) => {
            this.__parseComponent(node);
        });
    }

    __parseComponent(node) {

        // Data kept by the workflow tool
        // node.data;
        // Node name
        // node.name;
        // Node type
        // node.name;

        let component_data = {
            node: node,
            unique_config: "" + node.data["sst-component-name"] + node.index + "-config",
            unique_config_variable: "" + node.data["sst-component-name"] + node.index + "_config",
            unique_name: "" + node.data["sst-component-name"] + node.index,
        };
        this._components.push(component_data);

        let link_idx = 0;
        (node.allPorts || []).forEach( (port) => {
            (port.wires || []).forEach( (wire) => {
                // Get destination node and port
                let destination_port = wire.portEnd.node.index==node.index?wire.portStart:wire.portEnd;
                let destination_node = destination_port.node;

                // Store index of origin and destination
                let origin = [node.index, port.index];
                let destination = [destination_node.index, destination_port.index];

                // Setup variables with names and unique names
                let origin_name = "" + node.data["sst-component-name"];
                let origin_unique_name = "" + component_data.unique_name;
                let origin_port_name = "" + port.name;
                let origin_port_unique_name = "" + origin_unique_name + "_" + origin_port_name + link_idx;

                let destination_name = "" + destination_node.data["sst-component-name"];
                let destination_unique_name = "" + destination_name + destination_node.index;
                let destination_port_name = "" + destination_port.name;
                let destination_port_unique_name = "" + destination_unique_name + "_" + destination_port_name + link_idx;

                let link_unique_name = "" + origin_unique_name + "_" + destination_unique_name + "_link" + link_idx;

                let latency_origin_unique_name = origin_port_unique_name + "_latency" + link_idx;
                let latency_destination_unique_name = destination_port_unique_name + "_latency" + link_idx;

                let link_data = {
                    origin: origin,
                    destination: destination,
                    link_unique_name: link_unique_name,
                    latency_origin_unique_name: latency_origin_unique_name,
                    latency_destination_unique_name: latency_destination_unique_name,
                    origin_port_name: origin_port_name,
                    destination_port_name: destination_port_name,
                    origin_component_unique_name: origin_unique_name,
                    destination_component_unique_name: destination_unique_name
                };

                if (component_data.node.data["sst-is-subcomponent"]) {
                    component_data.subcomponent_of = {
                        name: destination_name,
                        unique_name: destination_unique_name,
                        port_name: destination_port.name,
                        index: wire.indexEnd
                    };
                }

                // Wires will be found twice, as are connected to both nodes.
                // There is a better way to do this for sure! but for now
                let exists = false;
                this._connections.forEach( (connection) => {
                    if (origin[0] === connection.destination[0] &&
                        origin[1] === connection.destination[1] &&
                        destination[0] === connection.origin[0] &&
                        destination[1] === connection.origin[1]) {
                        exists = true;
                    }
                });

                if (!exists && (port.data===undefined || !port.data.ignore)) {
                    this._connections.push(link_data);
                    link_idx++;
                }
            });
        });

    }

}

export class Graph2Simulation {
    constructor(nodes, sstInfo) {
        this.sstInfo = sstInfo;
        this._sst_graph = new SSTGraph(nodes, sstInfo);
    }

    get simulation() {
        // Start the file importing everything needed
        let simulation = "";
        simulation += "import sst\n";
        simulation += "import argparse\n";
        simulation += "import json\n\n";

        // Add the input parameter parser and arguments
        simulation += "parser = argparse.ArgumentParser()\n";
        simulation += "parser.add_argument('--output')\n";
        this._sst_graph.components.forEach( (component) => {
            let name = component.node.data["sst-component-name"];
            simulation += "parser.add_argument('--" + component.unique_config + "')\n";
        });
        simulation += "\n";

        this._sst_graph.components.forEach( (component) => {
            this.getFileParameters(component).forEach( (file) => {
                simulation += "parser.add_argument('--" + component.unique_name + "-" + file.name + "')\n";
            });
        });
        simulation += "\n";

        // Parse all inputs and load configuration files
        simulation += "args=parser.parse_args()\n";
        this._sst_graph.components.forEach( (component) => {
            let name = component.node.data["sst-component-name"];
            simulation += "with open(args." + component.unique_config_variable + ") as f:\n";
            simulation += "    " + component.unique_config_variable + " = json.load(f)\n";
            simulation += "\n";
        });
        simulation += "\n";

        // Add components to the simulation
        this._sst_graph.components.forEach( (component) => {
            let path = component.node.data["sst-component-path"];
            let is_subcomponent = component.node.data["sst-is-subcomponent"];
            if (!is_subcomponent) {
                simulation += "" + component.unique_name + "= sst.Component(\"" + component.unique_name + "\", \"" + path + "\")\n";
            }
        });
        simulation += "\n";

        // Only then, add subcomponents because they need the component variables to exist
        this._sst_graph.components.forEach( (component) => {
            let path = component.node.data["sst-component-path"];
            let is_subcomponent = component.node.data["sst-is-subcomponent"];
            if (is_subcomponent) {
                if (component.subcomponent_of !== undefined) {
                    simulation += "" + component.unique_name + " = " + component.subcomponent_of.unique_name + ".setSubComponent(\"" + component.subcomponent_of.port_name + "\", \"" + path + "\", " + component.subcomponent_of.index + ")\n";
                }
            }
        });
        simulation += "\n";

        // Add the loaded configuration parameters to each component
        // And add parameters that are an input file
        this._sst_graph.components.forEach( (component) => {
            simulation += "" + component.unique_name + ".addParams(" + component.unique_config_variable + ")\n";
            this.getFileParameters(component).forEach( (file) => {
                simulation += "" + component.unique_name + ".addParams({\n";
                simulation += "    \"" + file.name + "\": args." + component.unique_name + "_" + file.name + "\n";
                simulation += "})\n";
            });
        });
        simulation += "\n";

        this._sst_graph.connections.forEach( (connection) => {
            let origin = connection.origin;
            let destination = connection.destination;

            simulation += "" + connection.link_unique_name + " = sst.Link(\"" + connection.link_unique_name + "\")\n";
            simulation += "" + connection.latency_origin_unique_name + " = \"10ns\"\n";
            simulation += "" + connection.latency_destination_unique_name + " = \"10ns\"\n";
            simulation += "" + connection.link_unique_name + ".connect( (" +
                connection.origin_component_unique_name + ", \"" +
                connection.origin_port_name + "\", " +
                connection.latency_origin_unique_name + "), (" +
                connection.destination_component_unique_name + ", \"" +
                connection.destination_port_name + "\", " +
                connection.latency_destination_unique_name + ") )\n\n";
        });

        simulation += 'sst.setStatisticOutput("sst.statOutputCSV")\n';
        simulation += 'sst.setStatisticLoadLevel(7)\n';
        simulation += 'sst.setStatisticOutputOptions( {\n';
        simulation += '    "filepath"  : args.output,\n';
        simulation += '    "separator" : ", "\n';
        simulation += '    } )\n';
        simulation += 'sst.enableAllStatisticsForAllComponents()\n';

        return simulation;
    }

    get inputs() {
        let inputs = [];
        let configurations = this.configurations();

        Object.keys(configurations).forEach( (key) => {
            let input = {
                metadata: {
                    type: "configuration",
                    name: "Configure " + key,
                    subtype: "application/json",
                    schema: key + "_schema.json",
                    file: key + ".json"
                },
                schema: {
                    fileName: key + "_schema.json",
                    data: JSON.stringify(configurations[key], null, 2)
                }
            };
            inputs.push(input);
        });

        this.inputFiles().forEach( (file) => {
            let input = {
                metadata: {
                    type: "file",
                    name: file.name
                }
            };
            inputs.push(input);
        });

        return inputs;
    }

    inputFiles() {
        let inputs = [];
        this._sst_graph.components.forEach( (component) => {
            this.getFileParameters(component).forEach( (file) => {
                inputs.push(file);
            });
        });
        return inputs;
    }

    getFileParameters(component) {
        let files = [];
        let component_info = this.getComponentInfo(component);
        component_info.parameters.forEach( (parameter) => {
            if (parameter.type === "file") {
                let file = {};
                file.type = parameter.type;
                file.description = parameter.description;
                file.name = parameter.name;
                files.push(file);
            }
        });
        return files;
    }

    configurations() {
        let configurations = {};
        this._sst_graph.components.forEach( (component) => {
            let name = component.unique_name;
            configurations[name] = this.configurationSchemaJson(component);
        });
        return configurations;
    }

    configurationSchemaJson(component) {
        let configurations = {};
        let component_info = this.getComponentInfo(component);
        component_info.parameters.forEach( (parameter) => {
            if (!(parameter.deprecated || parameter.moved || parameter.type === "file")) {
                let configuration = {};

                configuration.type = parameter.type;
                configuration.description = parameter.description;

                let re = new RegExp("\(optional\)");

                if (parameter.description.toLowerCase().startsWith("optional")
                    || parameter.description.toLowerCase().match(re)
                    || parameter.default === ""
                    || parameter.default === undefined) {
                    configuration.optional = true;
                }

                // Just make EVERYTHING optional hahaha
                configuration.optional = true;

                if (parameter.default !== undefined) {
                    // Special case some interesting defaults that should be ignored
                    if (!(configuration.optional && parameter.default.toUpperCase && parameter.default === "")) {
                        configuration.default = parameter.default;
                    }
                }

                if (parameter.validations !== undefined) {
                    configuration.validations = parameter.validations;
                }

                configurations[parameter.name] = configuration;
            }
        });

        return configurations;
    }

    getComponentInfo(component) {
        let componentInfo;
        let componentName = component.node.data["sst-component-path"];
        componentInfo = this.sstInfo.getComponentByPath(componentName);
        if (componentInfo.length === 0) {
            // Try getting the subcomponent
            componentInfo = this.sstInfo.getSubcomponentByPath(componentName);
        }
        if (componentInfo.length === 0) {
            // Try getting the component directly
            componentInfo = this.sstInfo.getComponentByName(componentName);
        }
        if (componentInfo.length === 0) {
            // Try getting the subcomponent directly
            componentInfo = this.sstInfo.getSubcomponentByName(componentName);
        }
        if (componentInfo.length !== 0) {
            componentInfo=componentInfo[0];
        }
        return componentInfo;
    }
}

