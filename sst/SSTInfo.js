"use strict";

export class SSTInfo {
    constructor() {
        this.elements = [];
    }

    addElementsFromXML(sst_info, metadata) {
        let parser = new DOMParser();
        let info = parser.parseFromString(sst_info,"text/xml");
        let element_names = [];
        [...info.getElementsByTagName("Element")].forEach( (element) => {
            this.elements.push( new SSTElement(element, metadata) );
            element_names.push(element);
        });
    }

    addElement(element) {
        this.elements.push(element);
    }

    getElementByName(name) {
        return this.elements.filter( (element) => {
            return element.name === name;
        });
    }

    getComponentByPath(path) {
        let component_info;
        path = path.split(".");
        if (path.length > 1) {
            let element_info = this.getElementByName(path[0]);
            if (element_info) {
                component_info = element_info[0].getComponentByName(path[1]);
            }
        }
        return component_info;
    }

    getSubcomponentByPath(path) {
        let subcomponent_info;
        path = path.split(".");
        if (path.length > 1) {
            let element_info = this.getElementByName(path[0]);
            if (element_info) {
                subcomponent_info = element_info[0].getSubcomponentByName(path[1]);
            }
        }
        return subcomponent_info;
    }

    getComponentByName(name) {
        let ret=[];
        this.elements.forEach( (element) => {
            let components = element.getComponentByName(name);
            if (components) {
                ret = ret.concat(components);
            }
        });
        return ret;
    }

    getSubcomponentByName(name) {
        let ret=[];
        this.elements.forEach( (element) => {
            let subcomponents = element.getSubcomponentByName(name);
            if (subcomponents) {
                ret = ret.concat(subcomponents);
            }
        });
        return ret;
    }

    getElements() {
        return this.elements;
    }
}

class SSTElement {
    constructor(element, metadata) {
        this.metadata = metadata;
        this.element = element;
        this.index = element.getAttribute("Index");
        this.name = element.getAttribute("Name");
        this.description = element.getAttribute("Description");
        this.components = [];
        // this.events = [];
        // this.modules = [];
        this.subcomponents = [];
        // this.partitioners = [];
        // this.generators = [];
        [...element.getElementsByTagName("Component")].forEach( (component) => {
            this.components.push( new SSTComponent(component, metadata) );
        });
        // element.getElementsByTagName("Event").forEach( (event) => {
        //   this.events.push( new SSTEvent(event, metadata) );
        // });
        // element.getElementsByTagName("Module").forEach( (module) => {
        //   this.modules.push( new SSTModule(module, metadata) );
        // });
        [...element.getElementsByTagName("SubComponent")].forEach( (subcomponent) => {
            this.subcomponents.push( new SSTSubcomponent(subcomponent, metadata) );
        });
        // element.getElementsByTagName("Partitioner").forEach( (partitioner) => {
        //   this.partitioners.push( new SSTPartitioner(partitioner, metadata) );
        // });
        // element.getElementsByTagName("Generator").forEach( (generator) => {
        //   this.generators.push( new SSTGenerator(generator, metadata) );
        // });
    }

    getComponentByName(name) {
        return this.components.filter( (component) => {
            return component.name === name;
        });
    }

    getSubcomponentByName(name) {
        return this.subcomponents.filter( (subcomponent) => {
            return subcomponent.name === name;
        });
    }

    getComponents() {
        return [].concat(this.components).concat(this.subcomponents);
    }
}

class SSTComponent {
    constructor(component, metadata) {
        this.metadata = metadata;
        this.component = component;
        this.index = component.getAttribute("Index");
        this.name = component.getAttribute("Name");
        this.description = component.getAttribute("Description");
        this.category = component.getAttribute("Category");
        this.interface = component.getAttribute("Interface");
        this.parameters = [];
        this.ports = [];
        // this.statistics = [];
        this.subcomponent_slots = [];
        [...component.getElementsByTagName("Parameter")].forEach( (parameter) => {
            this.parameters.push( new SSTParameter(parameter) );
        });
        [...component.getElementsByTagName("Port")].forEach( (port) => {
            this.ports.push( new SSTPort(port) );
        });
        // component.getElementsByTagName("Statistic").forEach( (statistic) => {
        //   this.statistics.push( new SSTStatistic(statistic) );
        // });
        [...component.getElementsByTagName("SubComponentSlot")].forEach( (subcomponent_slot) => {
            this.subcomponent_slots.push( new SSTSubcomponentSlot(subcomponent_slot) );
        });
    }

    // Write whatever metadata we have in input
    appendMetadata(data) {
        return {
            ...data,
            ...this.metadata
        };
    }
}

class SSTSubcomponent extends SSTComponent { // Or is it the other way around?
    constructor(component, metadata) {
        super(component, metadata);
    }
}

class SSTSubcomponentSlot {
    constructor(slot, metadata) {
        this.metadata = metadata;
        this.index = slot.getAttribute("Index");
        this.name = slot.getAttribute("Name");
        this.description = slot.getAttribute("Description");
        this.interface = slot.getAttribute("Interface");
    }
}

class SSTPort {
    constructor(port, metadata) {
        this.metadata = metadata;
        this.index = port.getAttribute("Index");
        this.name = port.getAttribute("Name");
        this.description = port.getAttribute("Description");
        let event = [...port.getElementsByTagName("PortValidEvent")];
        if (event.length !== 0) {
            this.type = event[0].getAttribute("Event");
        }
    }
}

class SSTParameter {
    constructor(parameter, metadata) {
        this.metadata = metadata;
        this.parameter = parameter;
        this.index = parameter.getAttribute("Index");
        this._name = parameter.getAttribute("Name");
        this.description = parameter.getAttribute("Description");
        this._default = parameter.getAttribute("Default");
    }

    get name() {
        let re = new RegExp("^(?:\\(|[a-zA-Z0-9]+, )([a-zA-Z0-9]+)");
        let match = this._name.match(re);
        if (match) {
            return match[1];
        }
        return this._name;
    }

    get deprecated() {
        return this.description.includes("DEPRECATED");
    }

    get moved() {
        return this.description.includes("MOVED");
    }

    get type() {
        let re = new RegExp("^(?:\\(|[a-zA-Z0-9]+, )([a-zA-Z0-9]+)");
        let type = this.description.match(re);
        if (type) {
            switch (type[1]) {
                case "bool":
                    return "boolean";

                case "uint":
                case "int":
                    return "int";

                case "file":
                    return "file";

                case "string":
                case null:
                case undefined:
                default:
                    return "string";
            }
        }
        return "string";
    }

    get default() {
        // Up to at least 9.0
        if (this._default === "REQUIRED") {
            return undefined;
        }
        // At least after 13.0 does this
        if (this._default === "none") {
            return undefined;
        }

        let re = new RegExp("^(?:\\(|[a-zA-Z0-9]+, )([a-zA-Z0-9]+)");
        let type = this.description.match(re);
        if (type) {
            switch (type[1]) {
                case "boolean":
                    return (this._default ? true : false) || (this._default !== 0);

                case "uint":
                case "int":
                    if (isNaN(this._default)) {
                        return undefined;
                    }
                    return parseInt(this._default);

                case "string":
                case null:
                case undefined:
                default:
                    return this._default;
            }
        }

        return this._default;
    }

    get validations() {
        let re = new RegExp("^(?:\\(|[a-zA-Z0-9]+, )([a-zA-Z0-9]+)");
        let type = this.description.match(re);

        if (type) {
            switch (type[1]) {
                case "uint":
                    return [
                        {
                            "min": 0
                        }
                    ];

                case "int":
                case "boolean":
                case "string":
                case null:
                case undefined:
                default:
                    return undefined;
            }
        }

        return undefined;
    }
}

